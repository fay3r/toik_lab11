package com.example.toik11.repository;

import com.example.toik11.domain.dto.MovieDto;
import java.util.List;

public interface MovieRepository {

    List<MovieDto> getMovies();
}

package com.example.toik11.repository;

import com.example.toik11.domain.dto.MovieDto;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    private final List<MovieDto> movies;

    private Integer lastFreeIndex;
    public MovieRepositoryImpl() {
        lastFreeIndex=1;
        movies = new ArrayList();
        movies.add(new MovieDto(lastFreeIndex++,"Piraci z krzemowej doliny", 1999,"https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"));
        movies.add(new MovieDto(lastFreeIndex++,"Ja, robot", 2004,"https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"));
        movies.add(new MovieDto(lastFreeIndex++,"Kod nieśmiertelności", 2011,"https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"));
        movies.add(new MovieDto(lastFreeIndex++,"Ex Machina", 2015,"https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg"));
    }

    @Override
    public List<MovieDto> getMovies() {
        return movies;
    }
}

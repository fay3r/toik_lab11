package com.example.toik11.Controller;

import com.example.toik11.domain.dto.MovieDto;
import com.example.toik11.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/movies")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity getMovies() {
       // Map info = new HashMap();
        //info.put("movies",movieService.sendListToShow());
        return new ResponseEntity<>(movieService.sendListToShow(),HttpStatus.OK);
    }

}

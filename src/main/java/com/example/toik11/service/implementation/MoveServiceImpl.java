package com.example.toik11.service.implementation;

import com.example.toik11.repository.MovieRepository;
import com.example.toik11.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MoveServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;


    @Override
    public MovieRepository sendListToShow() {
        return movieRepository;
    }
}

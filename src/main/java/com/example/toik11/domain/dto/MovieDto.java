package com.example.toik11.domain.dto;

public class MovieDto {
    private int movieId;
    private String title;
    private int year;
    private String image;


    public MovieDto(int movieId, String title, int year, String image) {
        this.movieId = movieId;
        this.image = image;
        this.title = title;
        this.year = year;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }

}
